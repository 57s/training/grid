# Grid vs Flex

## Grid

- `repeat()` - функция, позволяющая повторять фрагменты.
- `minmax()` - функция, определяет диапазон размеров от _min_ до _max_
- `auto-fill` - занять все доступное пространство.
- `auto-fit` - занять все доступное пространство и растянуться.

### Центрирование

```css
.cards-list {
	display: grid;
	grid-template-columns: repeat(auto-fill, 300px);
	justify-content: center;
	gap: 50px 30px;
}
```

### Растягивание

```css
.cards-list {
	display: grid;
	grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
}
```

## Flex

Сложно сделать что бы карточки занимали все свободное место,
но при этом последняя занимала столько же место что и другие.

Так же сложно сделать что бы карточки центрировались,
но последняя карточка было под первым рядом.

```css
.cards-list {
	display: flex;
	flex-wrap: wrap;
	gap: 50px 30px;
}

.card {
	flex-basis: 300px;
}
```
